let input = document.querySelector('#input');
let form = document.querySelector('#form');
let tableBody = document.querySelector('#table-body');
let txt;

function renderTokens(){
  tokenize(
    txt,
    sqlSyntax,
    function(err, results = []){
      tableBody.innerHTML = '';
      results.map(function(token){
        tableBody.innerHTML += `
          <tr>
            <td>${token.type}</td>
            <td>${token.value}</td>
          </tr>
        `;
      });
    }
  );
}

function init(){
  input.value = '';
  txt = '';
}

form.addEventListener('submit', function(e){
  e.preventDefault();
  txt = input.value.trim();
  renderTokens();
});

init();